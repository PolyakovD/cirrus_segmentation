# README #

Cirrus_segmentation is a utility for training and evaluation 
of CNN segmentation for astronomy images.  
We trained the network to separate Galactic optical cirrus 
from background using deep images from SDSS Stripe 82 in g, r and i bands.

### Availability and citation
**To use our model one can make a fork of this repository.**  
**If you use our model for work/research presented in a publication (whether directly, or as a dependency to another package), 
we ask that you please cite the paper: [Prospects for future studies using deep imaging: analysis of individual Galactic cirrus filaments.](https://ui.adsabs.harvard.edu/abs/2023MNRAS.519.4735S/abstract)  
### Starting from June 2023, the actual version of the project is hosted on the https://gitlab.com/polyakovdmi93/cirrus_segmentation

### Dependencies and configuration
* `python>=3.6` is required.
* Required python packages and models listed in doc/requirements.txt. They can be installed using the **pip** package manager
* Also `pix2pix` models are required. They can be installed via `pip`.
```bash
    (my_env) ... $ pip install git+https://github.com/tensorflow/examples.git
```
* To use GPU one can [configure **tensorflow2.x.**](https://www.tensorflow.org/install/gpu)
* We strongly recommend working in a virtual environment created with **venv**, **pipenv**, or **virtualenv**.

### Getting start
Instructions for using the utility are contained in the directory `./doc/Instruction_eng.md`.

### Contacts
One can contact us to ask questions about the utility by email: **polyakovdmi93@gmail.com**