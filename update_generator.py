from __future__ import absolute_import, division, print_function, unicode_literals

import argparse

from utils.train.config_handler import json_train_config
from utils.parser_utils import str2bool
from utils.train.pipeline_handler import PipelineHandler


def update_by_json(config: dict, make_dataset=True):
    """
    Осуществляет создание генератора .png данных и при необходимости сохраняет .png данные на диске
    :param config: словарь, содержащий конфигурацию обучения
    :param make_dataset: булевый флаг, указывающий на необходимость генерации обучающей
                         и валидационной .png выборок на диске
    """
    pipeline_handler = PipelineHandler(config, test_metrics=False, make_dataset=make_dataset, reuse_generator=False)

    pipeline_handler.generate_png_dataset()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Script to update gemerator of .png data for cirrus segmentation")
    parser.add_argument("config", help="Configuration file (.json or .tsv file)", type=str)
    parser.add_argument("--make_dataset", help="Optional: Boolean flag for writing dataset on disk, default: true",
                        type=str2bool, default=True)

    args = parser.parse_args()

    config = args.config
    make_dataset = args.make_dataset

    print(f"CONFIG: {config}")
    print(f"make_dataset: {make_dataset}")

    update_by_json(json_train_config(config, save_config=False), make_dataset=make_dataset)
