import cv2
import json
import numpy as np
import tensorflow as tf
import time

from astropy.io import fits as pyfits
from operator import add
from os import listdir
from os.path import join, isdir, basename
from pathlib import Path

from utils.train.config_handler import read_json
from utils.train.metrics import create_mask, fast_tp_fp_fn_tn, make_report_from_class_metrics
from utils.pkl_utils import read_pickle

BANDS = ["g", "r", "i"]


class EvaluateHandler:
    def __init__(self, config: dict, model_path: str):
        """
        :param config: словарь, содержащий конфигурацию обучения
        :param model_path: путь до директории с tf моделью
        """
        self.config = config
        self.test_data_dir = join(config["test_dir"], "data")
        self.test_mask_dir = join(config["test_dir"], "results")
        self.device = "/device:GPU:0" if config["GPU"] else "/CPU:0"
        # Проверяем существование директорий с .fits-изображениями и масками
        if not isdir(self.test_mask_dir):
            raise FileExistsError(f"Directory with mask: {self.test_mask_dir} doesn't exists.")
        if not isdir(self.test_data_dir):
            raise FileExistsError(f"Directory with images: {self.test_data_dir} doesn't exists.")

        # загружаем модель
        self.model_path = model_path
        self.model_dir = Path(model_path).parent.absolute()
        self.model_name = basename(self.model_path)

        self.model = tf.keras.models.load_model(model_path)
        self.model.summary()
        # загружаем генератор и квантили
        generator_path = join(self.config["generator_dir"], "generator_" + self.config["dataset_name"] + ".pkl")
        generator = read_pickle(generator_path)
        self.quantile = generator.quantile

    @staticmethod
    def normalize(channel_paths: list, quantile: list) -> list:
        """
        Нормализует g, r, i каналы поля для дальнейшей загрузки в модель
        :param channel_paths: пути до .fits изображений каналов g, r, i
        :param quantile: список квантилей уровня 0.999 для полос g, r, i
        :return: список с каналами (каждый канал np.array, размер соответствует размеру поля)
        """
        normalize_channels = []
        for channel_path, q in zip(channel_paths, quantile):
            # для начала каждую карту фильтруем по 0 и значению квантили, чтобы она была положительной
            channel_fits = pyfits.open(channel_path)[0].data
            positive_mask = channel_fits >= 0
            quantile_mask = q * np.ones_like(channel_fits)
            filtered_fits = np.minimum(positive_mask * channel_fits + 1, quantile_mask)
            # далее преобразуем в логарифмический масштаб
            log_fits = np.log(filtered_fits)
            normalize_channels.append(cv2.normalize(log_fits, None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX,
                                                    dtype=cv2.CV_8U))
        return normalize_channels

    @staticmethod
    def predict_field(channel_paths: list, quantile: list, shape: list, scale: int, model, device: str):
        """
        Осуществляет предсказание маски для поля
        :param channel_paths: список с путями до .fits файлов с изображениями поля в полосах g, r, i
        :param quantile: список квантилей уровня 0.999 для полос g, r, i
        :param shape: список из пространственных размеров окон, подаваемых на вход нейронной сети
        :param scale: фактор отличающий размер окна на поле от окна, передаваемого в нейронную сеть:
                      window_shape = scale * shape
        :param model: tf-модель, которая осуществляет предсказание
        :param device: строка, задающая режим вычисления (на GPU или CPU), возможные значения: "/device:GPU:0"
                       или "/CPU:0"
        :return: предсказанная маска
        """
        normalize_fits = EvaluateHandler.normalize(channel_paths, quantile)

        image_shape = normalize_fits[0].shape
        window_shape = [x * scale for x in shape]
        left_xs = range(0, image_shape[1], window_shape[1])
        top_ys = range(0, image_shape[0], window_shape[0])

        pred_mask = np.ones(shape=image_shape)

        start_time = time.time()
        for top_y in top_ys:
            if top_y + window_shape[0] > image_shape[0]:
                y_slice = slice(image_shape[0] - window_shape[0], image_shape[0])
            else:
                y_slice = slice(top_y, top_y + window_shape[0])
            for left_x in left_xs:
                if left_x + window_shape[1] > image_shape[1]:
                    x_slice = slice(image_shape[1] - window_shape[1], image_shape[1])
                else:
                    x_slice = slice(left_x, left_x + window_shape[1])
                sliding_window = cv2.merge((normalize_fits[2][y_slice, x_slice],
                                            normalize_fits[1][y_slice, x_slice],
                                            normalize_fits[0][y_slice, x_slice]))

                # делаем rescale window_shape -> shape, чтобы передавать изображение в нейронную сеть
                if scale != 1:
                    sliding_window = cv2.resize(sliding_window, (shape[1], shape[0]))

                # не забываем про нормировку
                # sliding_window = sliding_window.astype(np.float32) / 255.0
                # классическая нормировка для ResNet и MobileNet к [-1, 1]
                sliding_window = sliding_window.astype(np.float32) / 255.0 * 2.0 - 1.0

                input_tensor_shape = tuple([1] + list(shape) + [3])
                with tf.device(device):
                    pred_sliding_mask = model.predict(sliding_window.reshape(input_tensor_shape))
                pred_sliding_mask = np.reshape(create_mask(pred_sliding_mask), shape)

                # перед тем, как записать фрагмент маски, возвращаем ей размер исходного фрагмента:
                # shape -> window_shape
                if scale != 1:
                    # Не забываем для интерполяции сменить тип с int64 на float32
                    pred_sliding_mask = cv2.resize(pred_sliding_mask.astype(np.float32),
                                                   (window_shape[1], window_shape[0]),
                                                   interpolation=cv2.INTER_AREA).astype(np.uint8)
                pred_mask[y_slice, x_slice] = pred_sliding_mask

        print(f"time of evaluation: {time.time() - start_time}")
        return pred_mask

    def _field_tp_fp_fn_tn(self, field_name: str):
        """
        Осуществляет предсказание циррусов для поля с именем field_name
        и возвращает словарь, где по ключу-классу возвращается TP, FP, FN, TN для этого класса
        :param field_name: имя поля, для которого делается предсказание
        :return: dict with TP, FP, FN, TN
        """
        # формируем путь до изображений поля
        channel_paths = [join(self.config["test_dir"], "data", f"{field_name}_{band}.rec.fits") for band in BANDS]

        # предсказываем маску для поля
        pred_mask = self.predict_field(channel_paths, self.quantile, self.config["shape"], self.config["scale"],
                                       self.model, self.device)

        # считываем истинную маску
        if len(self.config["target_names"]) == 2:
            true_mask = pyfits.open(join(self.test_mask_dir, field_name + ".fits"))[0].data.astype(np.uint8) % 2
        else:
            true_mask = pyfits.open(join(self.test_mask_dir, field_name + ".fits"))[0].data.astype(np.uint8)

        start = time.time()
        print(f"start class_metrics estimation for {field_name}")
        class_metrics = fast_tp_fp_fn_tn(true_mask, pred_mask.astype(np.uint8), list(range(self.config["n_class"])))
        print(f"class_metrics estimation has been completed in {time.time() - start} second")
        return class_metrics

    def evaluate(self):
        """
        Производит предсказание и вычисление метрик по всем полям.
        Полученный отчёт записывает в .tsv файл
        """
        n_class = self.config["n_class"]
        field_names = sorted([name.partition(".")[0] for name in listdir(self.test_mask_dir)])
        common_class_metrics = {key: [0, 0, 0, 0] for key in range(n_class)}
        for field_name, index in zip(field_names, range(len(field_names))):
            if index % 5 == 1:
                print(f"{index} fields have already prepared.")
            # делаем предсказание
            field_class_metrics = self._field_tp_fp_fn_tn(field_name)
            for class_id, metrics in field_class_metrics.items():
                common_class_metrics[class_id] = list(map(add, common_class_metrics[class_id], metrics))
        # запишем метрики
        test_report = make_report_from_class_metrics(common_class_metrics, self.config["target_names"])
        tsv_name = self.model_name + "_real_test_metrics.tsv"
        test_report.to_csv(join(self.model_dir, tsv_name), sep="\t", header=True, index=False,
                           encoding="utf-8")
        print(test_report)

    def update_config(self):
        """
        Если в конфигурации по которой обучалась модель (она записана в директории model_dir) нет поля test_dir,
        то оно записывается
        """
        old_config_name = self.model_name + "_config.json"
        old_config_path = join(self.model_dir, old_config_name)
        old_config = read_json(old_config_path)
        if "test_dir" not in old_config:
            old_config["test_dir"] = self.config["test_dir"]
            with open(old_config_path, mode="w", encoding="utf8") as writer:
                writer.write(json.dumps(old_config, indent=4, sort_keys=False))
