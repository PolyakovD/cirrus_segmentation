import pickle


def write_pickle(pkl_path: str, my_object):
    with open(pkl_path, "wb") as outfile:
        pickle.dump(my_object, outfile)


def read_pickle(pkl_path: str):
    """
    return representation of .pkl file as OrderedDict
    """
    with open(pkl_path, "rb") as file_object:
        raw_data = file_object.read()

    results = pickle.loads(raw_data)
    return results
