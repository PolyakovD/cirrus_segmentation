import tensorflow as tf

from tensorflow.keras.layers import BatchNormalization, Concatenate, Conv2D, Conv2DTranspose, Input, MaxPooling2D, \
    SpatialDropout2D


def encoder_mini_block(inputs, n_filters=32, dropout_prob=0.2, max_pooling=True, is_bn=False):
    conv = Conv2D(n_filters, 3, activation='relu', padding='same', kernel_initializer='HeNormal')(inputs)
    if is_bn:
        conv = BatchNormalization()(conv)
    conv = Conv2D(n_filters, 3, activation='relu', padding='same', kernel_initializer='HeNormal')(conv)

    conv = BatchNormalization()(conv)
    if dropout_prob > 0:
        conv = SpatialDropout2D(dropout_prob)(conv)
    if max_pooling:
        next_layer = MaxPooling2D(pool_size=(2, 2))(conv)
    else:
        next_layer = conv

    skip_connection = conv
    return next_layer, skip_connection


def decoder_mini_block(prev_layer_input, skip_layer_input, n_filters=32, dropout_prob=0.2, is_bn=False):
    up = Conv2DTranspose(n_filters, (3, 3), strides=(2, 2), padding='same')(prev_layer_input)
    merge = Concatenate()([up, skip_layer_input])

    conv = Conv2D(n_filters, 3, activation='relu', padding='same', kernel_initializer='HeNormal')(merge)
    if is_bn:
        conv = BatchNormalization()(conv)
    conv = Conv2D(n_filters, 3, activation='relu', padding='same', kernel_initializer='HeNormal')(conv)

    conv = BatchNormalization()(conv)
    if dropout_prob > 0:
        conv = SpatialDropout2D(dropout_prob)(conv)
    return conv


def original_unet_model(config):
    """
    Модель UNet
    Encoder: FCN
    Decoder: FCN
    Source page: https://medium.com/geekculture/u-net-implementation-from-scratch-using-tensorflow-b4342266e406
    :param config: config для запуска обучения
    """

    input_shape = list(config["shape"]) + [3]  # задаём размеры входного 3D тензора
    inputs = Input(shape=input_shape)

    # конфигурируем энкодер
    x = inputs
    current_size = input_shape[0]
    n_filters = 64
    skips = []
    while current_size > 31:
        x, skip_layer = encoder_mini_block(x, n_filters=n_filters, dropout_prob=-1)
        skips.append(skip_layer)
        n_filters *= 2
        current_size //= 2

    # конфигурируем декодера
    x = skips[-1]
    i = -2
    n_filters //= 4
    while i >= -len(skips):
        x = decoder_mini_block(x, skips[i], n_filters=n_filters, dropout_prob=-1)
        i -= 1
        n_filters //= 2

    # This is the last layer of the model
    # конволюция 1x1 для предсказания классов
    x = Conv2D(config["n_class"], 1, activation='relu')(x)

    model = tf.keras.Model(inputs=inputs, outputs=x)
    model.summary()

    model.compile(optimizer='adam',
                  loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  metrics=['accuracy'])
    return model


def original_unet_with_dropout_model(config):
    """
    Модель UNet
    Encoder: FCN
    Decoder: FCN + SpatialDropout2D
    Source page: https://medium.com/geekculture/u-net-implementation-from-scratch-using-tensorflow-b4342266e406
    :param config: config для запуска обучения
    """

    input_shape = list(config["shape"]) + [3]  # задаём размеры входного 3D тензора
    inputs = Input(shape=input_shape)

    # конфигурируем энкодер
    x = inputs
    current_size = input_shape[0]
    n_filters = 64
    skips = []
    while current_size > 31:
        x, skip_layer = encoder_mini_block(x, n_filters=n_filters)
        skips.append(skip_layer)
        n_filters *= 2
        current_size //= 2

    # конфигурируем энкодер
    x = skips[-1]
    i = -2
    n_filters //= 4
    while i >= -len(skips):
        x = decoder_mini_block(x, skips[i], n_filters=n_filters)
        i -= 1
        n_filters //= 2

    # This is the last layer of the model
    # конволюция 1x1 для предсказания классов
    x = Conv2D(config["n_class"], 1, activation='relu')(x)

    model = tf.keras.Model(inputs=inputs, outputs=x)
    model.summary()

    model.compile(optimizer='adam',
                  loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  metrics=['accuracy'])
    return model


def original_unet_with_bn_model(config):
    """
    Модель UNet
    Encoder: FCN + extra BatchNormalization layer
    Decoder: FCN + extra BatchNormalization layer
    Source page: https://medium.com/geekculture/u-net-implementation-from-scratch-using-tensorflow-b4342266e406
    :param config: config для запуска обучения
    """

    input_shape = list(config["shape"]) + [3]  # задаём размеры входного 3D тензора
    inputs = Input(shape=input_shape)

    # конфигурируем энкодер
    x = inputs
    current_size = input_shape[0]
    n_filters = 64
    skips = []
    while current_size > 31:
        x, skip_layer = encoder_mini_block(x, n_filters=n_filters, dropout_prob=-1, is_bn=True)
        skips.append(skip_layer)
        n_filters *= 2
        current_size //= 2

    # конфигурируем декодера
    x = skips[-1]
    i = -2
    n_filters //= 4
    while i >= -len(skips):
        x = decoder_mini_block(x, skips[i], n_filters=n_filters, dropout_prob=-1, is_bn=True)
        i -= 1
        n_filters //= 2

    # This is the last layer of the model
    # конволюция 1x1 для предсказания классов
    x = Conv2D(config["n_class"], 1, activation='relu')(x)

    model = tf.keras.Model(inputs=inputs, outputs=x)
    model.summary()

    model.compile(optimizer='adam',
                  loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  metrics=['accuracy'])
    return model
