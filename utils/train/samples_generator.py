import cv2
import numpy as np
import pathlib
import random

from astropy.io import fits as pyfits
from os.path import join, basename, exists
from os import listdir

MOBILENET_SHAPES = [(128, 128), (160, 160), (192, 192), (224, 224)]


class SamplesGenerator:
    def __init__(self, config: dict):
        """
        :param config: config для запуска обучения
        """
        self.config = config
        self.n_train = config["n_train"]
        self.n_valid = config["n_valid"]
        self.shape = config["shape"]
        self.scale = config["scale"]
        self.window_shape = [self.shape[0] * self.scale, self.shape[1] * self.scale]
        self.corners = {}
        channel_config = config["channel_config"]
        n_train_fields = config["n_train_fields"]
        # определим n_train_fields случайных полей из которых будут извлекаться окна для обучения
        np.random.seed(43)
        self.all_fits_names = [file.split(".")[0] for file in listdir(channel_config["mask_dir"])]
        self.train_fits_names = list(np.random.choice(self.all_fits_names, n_train_fields, replace=False))
        self.n_valid_fields = self.config["n_valid_fields"]
        self.masks_paths = {name: join(channel_config["mask_dir"], name + ".fits")
                            for name in self.all_fits_names}
        self.fits_paths = {name: [join(channel_config["channels_dir"], name
                                       + channel_config["channels_suffixes"][0]),
                                  join(channel_config["channels_dir"], name
                                       + channel_config["channels_suffixes"][1]),
                                  join(channel_config["channels_dir"], name
                                       + channel_config["channels_suffixes"][2])]
                           for name in self.all_fits_names}
        self.shapes = {name: pyfits.open(path)[0].data.shape
                       for name, path in self.masks_paths.items()}

        # динамический диапазон
        band_max = self._band_max()

        # Вычисление квартили уровня 0.999
        # для каждого из диапазонов по всем изображениям
        self._quantile(band_max)

    def _band_max(self):
        """
        Вычисляет глобальный минимум и максимум для выборки изображений
        (возвращает только глобальные максимумы)
        """
        band_max = [0, 0, 0]
        for name, paths in self.fits_paths.items():
            for i, path in zip([0, 1, 2], paths):
                image = pyfits.open(path)[0].data
                band_max[i] = max(np.max(image), band_max[i])
                # print(basename(path))
                # print(f"max: {np.max(image)}, min: {np.min(image)}")
        print(f"g_max: {band_max[0]}, r_max: {band_max[1]}, i_max: {band_max[2]}")
        return band_max

    def _quantile(self, band_max, q=0.999):
        """
        Вычисление квартили уровня q для каждого из диапазонов по всем изображениям
        """
        self.quantile = band_max
        hists = [None, None, None]  # g, r, i bands
        for name, paths in self.fits_paths.items():
            for i, path in zip([0, 1, 2], paths):
                image = pyfits.open(path)[0].data
                hist, bins = np.histogram(image, int(band_max[i]),
                                          [0, int(band_max[i])])
                if hists[i] is None:
                    hists[i] = hist
                else:
                    hists[i] += hist
        for i in [0, 1, 2]:
            hist_sum = np.sum(hists[i])
            sum_q = hist_sum * q
            j = len(hists[i]) - 1
            print(f"i: {i}, j: {j}")
            while hist_sum > sum_q:
                hist_sum -= hists[i][j]
                j -= 1
            self.quantile[i] = j + 1
        print(self.quantile)
        print(f"quantile: {q}")
        print(f"g {self.quantile[0]}")
        print(f"r: {self.quantile[1]}")
        print(f"i: {self.quantile[2]}")

    def generate_corners(self, seed=43):
        """
        Генерируем набор угловых координат для окон на каждом изображении
        :param seed: seed для датчика случайных чисел
        """
        random.seed(seed)
        for name in self.all_fits_names:
            if name in self.train_fits_names:
                self.corners[name] = self._corners_from_image(name, self.n_train)
            else:
                self.corners[name] = self._corners_from_image(name, self.n_valid)

    def _corners_from_image(self, image_name: str, n_windows: int):
        """
        Генерирует набор координат углов окон (левый верхний пиксель) для
        заданного поля, размер окна self.shape * self.scale
        :param image_name: имя поля (соответствует имени маски без расширения)
        :param n_windows: количество окон для генерации на поле
        """
        image_shape = self.shapes[image_name]
        if self.window_shape[0] > image_shape[0] or self.window_shape[1] > image_shape[1]:
            raise ValueError(f"Shape of window {self.window_shape} is bigger, than shape of image {image_shape}")

        xrange = range(0, image_shape[0] - self.window_shape[0])
        yrange = range(0, image_shape[1] - self.window_shape[1])
        return random.sample([[x, y] for x in xrange for y in yrange], n_windows)

    def generate_png_datasets(self, train_data_path, valid_data_path):
        """
        Создаём тренировочную и тестовую выборки на диске
        :param train_data_path: путь до директории с обучающей выборкой окон
        :param valid_data_path: путь до директории с валидационной выборкой окон
        """
        new_paths = [join(train_data_path, "masks"), join(train_data_path, "images"),
                     join(valid_data_path, "masks"), join(valid_data_path, "images")]
        for path in new_paths:
            if not exists(path):
                pathlib.Path(path).mkdir(parents=True, exist_ok=True)

        for name in self.all_fits_names:
            if name in self.train_fits_names:
                self._png_dataset_from_field(name, train_data_path)
            else:
                self._png_dataset_from_field(name, valid_data_path)

    def _png_dataset_from_field(self, name, data_path):
        """
        Добавляет окна с одного поля в обучающую или валидационную выборку окон
        :param name: имя поля (соответствует имени маски без расширения)
        :param data_path: путь до директории, куда будут записаны окна (train_data_path или valid_data_path)
        """
        normalize_fits = []
        for channel_path, index in zip(self.fits_paths[name], [0, 1, 2]):
            # для начала каждую карту фильтруем по 0 и значению квантили,
            # чтобы она была положительной
            channel_fits = pyfits.open(channel_path)[0].data
            positive_mask = channel_fits >= 0
            quantile_mask = self.quantile[index] * np.ones_like(channel_fits)
            filtered_fits = np.minimum(positive_mask * channel_fits + 1,
                                       quantile_mask)
            # далее преобразуем в логарифмический масштаб
            log_fits = np.log(filtered_fits)
            normalize_fits.append(cv2.normalize(log_fits, None, alpha=0, beta=255,
                                                norm_type=cv2.NORM_MINMAX,
                                                dtype=cv2.CV_8U))
        # тип маски > f4
        image_mask = pyfits.open(self.masks_paths[name])[0].data
        image_mask = image_mask % 256
        # теперь запишем картинки и маски на диск
        corners = self.corners[name]
        for i, corner in zip(range(len(corners)), corners):
            # запишем картинки
            x_slice = slice(corner[0], corner[0] + self.window_shape[0])
            y_slice = slice(corner[1], corner[1] + self.window_shape[1])
            sliding_window = cv2.merge((normalize_fits[0][x_slice, y_slice],
                                        normalize_fits[1][x_slice, y_slice],
                                        normalize_fits[2][x_slice, y_slice]))
            # вторым аргументом принимает новый размер,
            # пространственные размеры в порядке w, h
            resized_window = cv2.resize(sliding_window, (self.shape[1], self.shape[0]))
            cv2.imwrite(join(data_path, "images", f"{name}_{i}.png"),
                        resized_window)
            # запишем маски
            resized_mask = cv2.resize(image_mask[x_slice, y_slice], (self.shape[1], self.shape[0]),
                                      interpolation=cv2.INTER_AREA)
            cv2.imwrite(join(data_path, "masks", f"mask_{name}_{i}.png"),
                        resized_mask)

    def get_n_train_fields(self) -> int:
        return self.config["n_train_fields"]

    def get_n_valid_fields(self) -> int:
        return self.n_valid_fields


def get_image_and_mask_paths(data_path: str, seed=43) -> tuple:
    """
    Возвращает кортеж из двух списков: пути до окон и соответствующих масок
    :param data_path: путь до директории с данными (.png изображения в поддиректории images
                      и маски в поддиректории masks)
    :param seed: зерно датчика случайных чисел
    """
    random.seed(seed)

    image_dir = join(data_path, "images")
    mask_dir = join(data_path, "masks")
    png_names = listdir(image_dir)
    # пошафлим картинки
    png_names = random.sample(png_names, len(png_names))

    image_paths = [join(image_dir, file) for file in png_names]
    mask_paths = [join(mask_dir, "mask_" + file) for file in png_names]
    return image_paths, mask_paths
