import numpy as np
import pandas as pd
import tensorflow as tf
import time


def create_mask(pred_mask):
    pred_mask = tf.argmax(pred_mask, axis=-1)
    pred_mask = pred_mask[..., tf.newaxis]
    return pred_mask[0]


def calculate_result(model, ds, config, n_batch: int) -> tuple:
    """
    Создаёт таблицу с результатами сегментации по предложенному набору данных
    :param model: tf модель, вычисляющая результат
    :param ds: tf dataset, на котором будет вычислен результат
    :param config: config для запуска обучения
    :param n_batch: количество батчей, которые нужно взять из выборки ds
    :return: кортеж из y_true и y_pred (np.array, dtype=np.int8)
    """
    device = "/CPU:0"
    if config["GPU"]:
        device = "/device:GPU:0"

    with tf.device(device):
        start_time = time.time()
        true_labels = np.array([], dtype=np.int8)
        pred_labels = np.array([], dtype=np.int8)
        image_shape = tuple([1] + list(config["shape"]) + [3])
        mask_shape = tuple([1] + list(config["shape"]) + [1])
        ind = 0
        for image_batch, mask_batch in ds.take(n_batch):
            print(f"batch number: {ind}")
            ind += 1
            for i in range(len(image_batch)):
                image = tf.reshape(image_batch[i], image_shape)
                # TODO: не понятно зачем здесь reshape
                true_mask = tf.reshape(mask_batch[i], mask_shape)
                pred_mask = model.predict(image, verbose=0)
                pred_mask = create_mask(pred_mask)

                true_labels = np.concatenate((true_labels, true_mask.numpy()[0].astype(np.int8).flatten()), axis=None)
                pred_labels = np.concatenate((pred_labels, pred_mask.numpy().astype(np.int8).flatten()), axis=None)
        print(f"evaluation time: {time.time() - start_time}")
        return true_labels, pred_labels


def fast_tp_fp_fn_tn(y_true, y_pred, class_ids: list) -> dict:
    """
    Вычисляет TP, FP, FN, TN для для классов из class_ids,
    где ключами являются классы, а значениями являются списки вида [TP, FP, FN, TN]
    :param y_true: numpy-array с оригинальной предобработанной маской
    :param y_pred: numpy-array с предсказанной маской
    :param class_ids: список классов, для которых рассчитывается iou
    """
    class_metrics = {}
    for class_id in class_ids:
        tp = np.count_nonzero(np.logical_and(y_true == class_id,
                                             y_pred == class_id))
        fp = np.count_nonzero(np.logical_and(y_true != class_id,
                                             y_pred == class_id))
        fn = np.count_nonzero(np.logical_and(y_true == class_id,
                                             y_pred != class_id))
        tn = np.count_nonzero(np.logical_and(y_true != class_id,
                                             y_pred != class_id))
        class_metrics[class_id] = [tp, fp, fn, tn]
    return class_metrics


def fast_make_report(y_true, y_pred, target_names: list):
    """
    Создаёт отчёт с метриками по результатам предсказаний
    :param y_true: истинные лейблы
    :param y_pred: предсказанные лейблы
    :param target_names: список имён классов
                         (в порядке возрастания классов от 0 до n_class)
    """
    n_class = len(target_names)

    class_metrics = fast_tp_fp_fn_tn(y_true, y_pred, list(range(n_class)))

    return make_report_from_class_metrics(class_metrics, target_names)


def make_report_from_class_metrics(class_metrics: dict, target_names: list):
    """
    Создаёт отчёт с метриками по TP, FP, FN и TN для классов
    :param class_metrics: словарь метрик классов см. _fast_tp_fp_fn_tn
    :param target_names: список имён классов
                         (в порядке возрастания классов от 0 до n_class)
    """
    n_class = len(class_metrics)
    n_sample = sum(class_metrics[0])
    n_classes = [class_metrics[i][0] + class_metrics[i][2] for i in range(n_class)]

    # начнём c accuracy
    accuracy = [-1] * len(target_names) + [sum([value[0] for value in
                                                class_metrics.values()]) / n_sample]
    # теперь recall
    recall = []
    recall_w_avg = 0
    for class_id in range(n_class):
        if class_metrics[class_id][0] != 0:
            recall.append(class_metrics[class_id][0] / (class_metrics[class_id][0] +
                                                        class_metrics[class_id][2]))
        else:
            recall.append(0)
        recall_w_avg += recall[-1] * n_classes[class_id] / n_sample
    recall.append(recall_w_avg)

    # теперь precision
    precision = []
    precision_w_avg = 0
    for class_id in range(n_class):
        if class_metrics[class_id][0] != 0:
            precision.append(class_metrics[class_id][0] / (class_metrics[class_id][0] +
                                                           class_metrics[class_id][1]))
        else:
            precision.append(0)
        precision_w_avg += precision[-1] * n_classes[class_id] / n_sample
    precision.append(precision_w_avg)

    # теперь f1-score
    f1_score = []
    f1_score_w_avg = 0
    for class_id in range(n_class):
        if (precision[class_id] + recall[class_id]) != 0:
            f1_score.append(2 * precision[class_id] * recall[class_id] /
                            (precision[class_id] + recall[class_id]))
        else:
            f1_score.append(0)
        f1_score_w_avg += f1_score[-1] * n_classes[class_id] / n_sample
    f1_score.append(f1_score_w_avg)

    # теперь iou
    iou = []
    for class_id in range(n_class):
        if class_metrics[class_id][0] != 0:
            iou.append(class_metrics[class_id][0] / (class_metrics[class_id][0] +
                                                     class_metrics[class_id][1] +
                                                     class_metrics[class_id][2]))
        else:
            iou.append(0)
    iou.append(-1)

    classes = target_names + ["weighted avg"]

    return pd.DataFrame({"classes": classes, "precision": precision,
                         "recall": recall, "f1-score": f1_score,
                         "accuracy": accuracy, "IoU": iou})
