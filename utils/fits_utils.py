from astropy.io import fits as pyfits
from os.path import exists


def save_fits(image, output_path: str, header=None):
    hdu = pyfits.PrimaryHDU(image)
    if header:
        hdu.header = header
    if exists(output_path):
        hdu.writeto(output_path, overwrite=True)
    else:
        hdu.writeto(output_path)
