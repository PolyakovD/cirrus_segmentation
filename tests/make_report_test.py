import numpy as np
import pandas as pd
import pytest
import time

from math import isclose
from sklearn.metrics import classification_report

from iou_test import jaccard_iou
from utils.train.metrics import fast_make_report

EPS = 1.0e-9


def make_report(y_true, y_pred, target_names: list):
    """
    Создаёт отчёт с метриками по результатам предсказаний
    :param y_true: истинные лейблы
    :param y_pred: предсказанные лейблы
    :param target_names: список имён классов (в порядке номеров лейблов от 0 до n_class)
    """
    # для np.array вычисления быстрее с помощью jaccard_iou, нежели рукописного iou
    class_iou = jaccard_iou(y_true, y_pred)

    test_report = classification_report(y_true, y_pred, digits=3,
                                        output_dict=True,
                                        target_names=target_names)
    classes = target_names + ["weighted avg"]
    precision = [test_report[target_class]["precision"] for target_class in classes]
    recall = [test_report[target_class]["recall"] for target_class in classes]
    f1_score = [test_report[target_class]["f1-score"] for target_class in classes]
    accuracy = [-1] * len(target_names) + [test_report["accuracy"]]
    iou = [class_iou.get(class_label, -1) for class_label in range(len(target_names))] + [-1]
    return pd.DataFrame({"classes": classes, "precision": precision,
                         "recall": recall, "f1-score": f1_score,
                         "accuracy": accuracy, "IoU": iou})


def test_numpy_small_mutual_report():
    np.random.seed(43)
    for i in range(100):
        n_class = np.random.choice(np.array(range(1, 10)))
        first = np.array(range(n_class)).astype(np.int8)
        y_true = np.random.choice(n_class, 1000).astype(np.int8)
        y_true = np.concatenate((y_true, first))
        y_pred = np.random.choice(n_class, y_true.size).astype(np.int8)
        target_names = [str(i) for i in range(n_class)]

        default_report = make_report(y_true, y_pred, target_names)[["precision", "recall",
                                                                    "f1-score", "accuracy", "IoU"]].to_numpy()
        fast_report = fast_make_report(y_true, y_pred, target_names)[["precision", "recall",
                                                                      "f1-score", "accuracy", "IoU"]].to_numpy()
        for col_num in range(5):
            for j in range(n_class + 1):
                pytest.assume(isclose(default_report[j, col_num], fast_report[j, col_num], abs_tol=EPS))


def test_numpy_mutual_report():
    np.random.seed(43)
    y_true = np.random.choice(2, 100000000).astype(np.int8)
    y_pred = np.random.choice(2, 100000000).astype(np.int8)
    target_names = ["0", "1"]

    start_time = time.time()
    default_report = make_report(y_true, y_pred, target_names)
    print(f"\nreport making using make_report was completed in {time.time() - start_time} seconds")
    print(f"default report\n {default_report}")
    np_default_report = default_report[["precision", "recall", "f1-score", "accuracy", "IoU"]].to_numpy()

    start_time = time.time()
    fast_report = fast_make_report(y_true, y_pred, target_names)
    print(f"\nreport making using fast_make_report was completed in {time.time() - start_time} seconds")
    print(f"fast report\n {fast_report}")
    np_fast_report = fast_report[["precision", "recall", "f1-score", "accuracy", "IoU"]].to_numpy()

    for col_num in range(5):
        for i in range(3):
            pytest.assume(isclose(np_default_report[i, col_num], np_fast_report[i, col_num], abs_tol=EPS))
