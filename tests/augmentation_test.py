import os
import pytest

from os.path import join

from utils.train.config_handler import json_train_config
from utils.train.pipeline_handler import PipelineHandler


@pytest.fixture
def shadow_pipeline_handler():
    config_path = "../training_configs/demo/UNet_shallow_decoder_128_20x500_aug_demo_train.json"
    config = json_train_config(config_path)
    pipeline_handler = PipelineHandler(config)
    pipeline_handler.config["batch_size"] = 1
    return pipeline_handler


def test_augmentation(shadow_pipeline_handler):
    data_path = "./test_png_dataset"
    test_ds = shadow_pipeline_handler._make_tf_datasets(data_path, is_train=False, is_augmentation=True)
    print(f"len(list(test_ds)) {len(list(test_ds))}")
    assert (len(list(test_ds)) == 8 * len(os.listdir(join(data_path, "images"))))
