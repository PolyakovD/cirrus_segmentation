import numpy as np
import pytest

from math import isclose
from sklearn.metrics import confusion_matrix

from make_report_test import make_report

EPS = 1.0e-7


@pytest.fixture
def small_confusion_matrix():
    return np.array([[2, 1, 1], [1, 3, 0], [0, 1, 2]])


@pytest.fixture
def small_y_true():
    return [0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2]


@pytest.fixture
def small_y_pred():
    return [0, 0, 2, 1, 1, 1, 1, 0, 2, 2, 1]


def test_confusion_matrix(small_y_true, small_y_pred, small_confusion_matrix):
    assert (np.array_equal(confusion_matrix(small_y_true, small_y_pred), small_confusion_matrix))
