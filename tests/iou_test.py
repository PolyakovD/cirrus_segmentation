import numpy as np
import pytest
import time

from math import isclose
from sklearn.metrics import confusion_matrix, jaccard_score

EPS = 1.0e-7


def iou(y_true, y_pred):
    """
    Вычисляет IoU по истинным и предсказанным лейблам
    :param y_true: истинные лейблы
    :param y_pred: предсказанные лейблы
    """
    class_iou = {}
    for class_label in set(y_true):
        tp = 0
        fn = 0
        fp = 0
        for y_t, y_p in zip(y_true, y_pred):
            if y_t == class_label:
                if y_p == class_label:
                    tp += 1
                else:
                    fn += 1
            elif y_p == class_label:
                fp += 1
        class_iou[class_label] = tp / (tp + fn + fp)
    return class_iou


def jaccard_iou(y_true, y_pred) -> dict:
    """
    Вычисляет IoU по истинным и предсказанным лейблам
    :param y_true: истинные лейблы
    :param y_pred: предсказанные лейблы
    """
    j_scores = jaccard_score(y_true, y_pred, average=None)
    return {key: j_scores[key] for key in range(len(j_scores))}


def iou_from_matrix(c_matrix) -> list:
    """
    Вычисляет IoU по confusion_matrix
    :param c_matrix: confusion_matrix
    :return: список iou для классов
    """
    n_class = c_matrix.shape[0]
    class_iou = []
    for i in range(n_class):
        tp = c_matrix[i, i]
        fn = c_matrix[i].sum() - tp
        fp = c_matrix[:, i].sum() - tp
        if (tp + fn + fp) > 0:
            class_iou.append(tp / (tp + fn + fp))
        else:
            class_iou.append(-1)
    return class_iou


def fast_iou(y_true, y_pred, class_ids: list):
    """
    Вычисляет IoU для классов из class_ids
    :param y_true: numpy-array с оригинальной предобработанной маской
    :param y_pred: numpy-array с предсказанной маской
    :param class_ids: список классов, для которых рассчитывается iou
    """
    class_iou = {}
    for class_id in class_ids:
        tp = np.count_nonzero(np.logical_and(y_true == class_id,
                                             y_pred == class_id))
        tp_fp_fn = np.count_nonzero(np.logical_or(y_true == class_id,
                                                  y_pred == class_id))
        class_iou[class_id] = (tp / (tp_fp_fn))
    return class_iou


@pytest.fixture
def small_y_true():
    return [0, 0, 0, 0, 1, 0, 1, 1, 1]


@pytest.fixture
def small_y_pred():
    return [0, 0, 0, 1, 0, 1, 0, 1, 1]


def test_primitive_iou(small_y_true, small_y_pred):
    """
    Для использования assume необходимо установить plugin:
    pip install pytest-assume
    """
    pytest.assume(isclose(iou(small_y_true, small_y_pred)[0], 0.4285714, abs_tol=EPS))
    pytest.assume(isclose(iou(small_y_true, small_y_pred)[1], 0.3333333, abs_tol=EPS))


def test_jaccard_iou(small_y_true, small_y_pred):
    pytest.assume(isclose(jaccard_iou(small_y_true, small_y_pred)[0], 0.4285714, abs_tol=EPS))
    pytest.assume(isclose(jaccard_iou(small_y_true, small_y_pred)[1], 0.3333333, abs_tol=EPS))


def test_iou_from_matrix(small_y_true, small_y_pred):
    c_matrix = confusion_matrix(small_y_true, small_y_pred)
    pytest.assume(isclose(iou_from_matrix(c_matrix)[0], 0.4285714, abs_tol=EPS))
    pytest.assume(isclose(iou_from_matrix(c_matrix)[1], 0.3333333, abs_tol=EPS))


def test_fast_iou(small_y_true, small_y_pred):
    pytest.assume(isclose(fast_iou(np.array(small_y_true), np.array(small_y_pred), [0, 1])[0], 0.4285714, abs_tol=EPS))
    pytest.assume(isclose(fast_iou(np.array(small_y_true), np.array(small_y_pred), [0, 1])[1], 0.3333333, abs_tol=EPS))


def test_numpy_small_mutual_iou():
    np.random.seed(43)
    for i in range(100):
        n_class = np.random.choice(np.array(range(1, 10)))
        first = np.array(range(n_class)).astype(np.int8)
        y_true = np.random.choice(n_class, 1000).astype(np.int8)
        y_true = np.concatenate((y_true, first))
        y_pred = np.random.choice(n_class, y_true.size).astype(np.int8)

        my_fast_iou = fast_iou(y_true, y_pred, list(range(n_class)))
        jacc_iou = jaccard_iou(y_true, y_pred)
        for class_id in range(n_class):
            pytest.assume(isclose(my_fast_iou[class_id], jacc_iou[class_id], abs_tol=EPS))


def test_numpy_mutual_iou():
    np.random.seed(43)
    y_true = np.random.choice(2, 100000000).astype(np.int8)
    y_pred = np.random.choice(2, 100000000).astype(np.int8)

    start_time = time.time()
    my_fast_iou = fast_iou(y_true, y_pred, [0, 1])
    print(f"\niou estimation using fast_iou was completed in {time.time() - start_time} seconds")
    print(f"my_iou {my_fast_iou}")

    start_time = time.time()
    jacc_iou = jaccard_iou(y_true, y_pred)
    print(f"\niou estimation via sklearn.metrics.jaccard_score was completed in {time.time() - start_time} seconds")
    print(f"jacc_iou {jacc_iou}")

    pytest.assume(isclose(my_fast_iou[0], jacc_iou[0], abs_tol=EPS))
    pytest.assume(isclose(my_fast_iou[1], jacc_iou[1], abs_tol=EPS))
