from __future__ import absolute_import, division, print_function, unicode_literals

import argparse

from utils.predict.evaluate_handler import EvaluateHandler
from utils.train.config_handler import json_train_config


def evaluate_by_json(config: dict, model_path: str):
    """
    Осуществляет прогонку модели сегментации по конфигурации на тестовых данных и выдаёт метрики,
    а также переписывает конфигурацию (добавляет поле "test_dir", если его не было)
    :param config: словарь, содержащий конфигурацию обучения
    :param model_path: путь до директории с весами модели
    """
    evaluate_handler = EvaluateHandler(config, model_path)
    evaluate_handler.evaluate()
    evaluate_handler.update_config()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Script to evaluate metrics for test fields")
    parser.add_argument("config", help="Configuration file (.json file)", type=str)
    parser.add_argument("model_path", help="Directory with model", type=str)

    args = parser.parse_args()

    config = args.config
    model_path = args.model_path

    print(f"CONFIG: {config}")

    evaluate_by_json(json_train_config(config, save_config=False), model_path)
